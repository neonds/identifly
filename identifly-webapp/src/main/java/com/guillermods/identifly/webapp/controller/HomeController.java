/**
 * Copyright (C) 2016 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.identifly.webapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Guillermo B Díaz Solís
 * @since 25 de ene. de 2016
 */

@Controller
@RequestMapping("/home")
public class HomeController {

	@RequestMapping("/")
	public String index(){
		
		return "home/index";
	}
}
