/**
 * Copyright (C) 2016 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.identifly.webapp.security.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * @author Guillermo B Díaz Solís
 * @since 25 de ene. de 2016
 */
public class WebSecurityInitializer extends AbstractSecurityWebApplicationInitializer {

}
