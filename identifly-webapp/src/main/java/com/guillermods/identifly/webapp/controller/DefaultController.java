/**
 * Copyright (C) 2016 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.identifly.webapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Guillermo B Díaz Solís
 * @since 28 de ene. de 2016
 */
@Controller()
public class DefaultController {

	@RequestMapping("/")
	public String index(){
		return "redirect:/home/";
	}
}
