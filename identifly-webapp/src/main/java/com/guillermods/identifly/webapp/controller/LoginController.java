/**
 * Copyright (C) 2016 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.identifly.webapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.guillermods.identifly.thymeleaf.spring.support.Layout;

/**
 * @author Guillermo B Díaz Solís
 * @since 27 de ene. de 2016
 */


@Controller
@Layout(value = "layouts/login-layout")
public class LoginController {

	@RequestMapping("/login")
	public String index(){
		return "/login/index";
	}
	
	@RequestMapping("/logout")
	public String logout(){
		return "/login/index";
	}
}
