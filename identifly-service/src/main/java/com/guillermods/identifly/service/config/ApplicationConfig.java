/**
 * Copyright (C) 2016 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.identifly.service.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.guillermods.identifly.repository.config.DataBaseConfig;

/**
 * @author Guillermo B Díaz Solís
 * @since 25 de ene. de 2016
 */
@Configuration
@ComponentScan(basePackages = { "com.guillermods.identifly.service.impl" })
@Import(value = { DataBaseConfig.class })
public class ApplicationConfig {

}
