/**
 * Copyright (C) 2016 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.identifly.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * @author Guillermo B Díaz Solís
 * @since 28 de ene. de 2016
 * Use this class just for demos
 */
@Service("UserDetailsInMemoryServiceImpl")
public class UserDetailsInMemoryServiceImpl implements UserDetailsService{

	/* (non-Javadoc)
	 * @see org.springframework.security.core.userdetails.UserDetailsService#loadUserByUsername(java.lang.String)
	 */
	private HashMap<String, UserDetails> users;
	
	public UserDetailsInMemoryServiceImpl(){
		users = new HashMap<>();
		
		UserInMemory userA = new UserInMemory("memo", "123");
		UserInMemory userB = new UserInMemory("eliza", "123");
		
		users.put(userA.getUsername(), userA );
		users.put(userB.getUsername(), userB );
		
	}
		
	
	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		return users.get(userName);
	}
	
	class UserInMemory implements UserDetails {

		
		private static final long serialVersionUID = 1L;
		private String username;
		private String password;

		public UserInMemory(String username, String password) {
			this.username = username;
			this.password = password;
			
		}
		
		/* (non-Javadoc)
		 * @see org.springframework.security.core.userdetails.UserDetails#getAuthorities()
		 */
		@Override
		public Collection<? extends GrantedAuthority> getAuthorities() {
			GrantedAuthority granted = () -> "NORMAL";
			List<GrantedAuthority> granteds = new ArrayList<>();
			granteds.add(granted);
			return granteds;
		}

		/* (non-Javadoc)
		 * @see org.springframework.security.core.userdetails.UserDetails#getPassword()
		 */
		@Override
		public String getPassword() {
			// TODO Auto-generated method stub
			return this.password;
		}

		/* (non-Javadoc)
		 * @see org.springframework.security.core.userdetails.UserDetails#getUsername()
		 */
		@Override
		public String getUsername() {
		
			return this.username;
		}

		/* (non-Javadoc)
		 * @see org.springframework.security.core.userdetails.UserDetails#isAccountNonExpired()
		 */
		@Override
		public boolean isAccountNonExpired() {
			// TODO Auto-generated method stub
			return true;
		}

		/* (non-Javadoc)
		 * @see org.springframework.security.core.userdetails.UserDetails#isAccountNonLocked()
		 */
		@Override
		public boolean isAccountNonLocked() {
			// TODO Auto-generated method stub
			return true;
		}

		/* (non-Javadoc)
		 * @see org.springframework.security.core.userdetails.UserDetails#isCredentialsNonExpired()
		 */
		@Override
		public boolean isCredentialsNonExpired() {
			// TODO Auto-generated method stub
			return true;
		}

		/* (non-Javadoc)
		 * @see org.springframework.security.core.userdetails.UserDetails#isEnabled()
		 */
		@Override
		public boolean isEnabled() {
			// TODO Auto-generated method stub
			return true;
		}
		
	}

}
